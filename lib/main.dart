import 'package:flutter/material.dart';

import './screens/tabs/tabs.screen.dart';
import './screens/categories/categories.screen.dart';
import './screens/meals/category-meals.screen.dart';
import './screens/details/details.screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        accentColor: Colors.purple,
        canvasColor: Colors.grey,
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              body1: TextStyle(
                color: Colors.white70,
              ),
              title: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/',
      routes: {
        '/': (ctx) => TabScreen(),
        MealDetailScreen.route: (ctx) => MealDetailScreen(),
        CategoryMealsScreen.route: (ctx) => CategoryMealsScreen(),
      },
      onUnknownRoute: (_) =>
          MaterialPageRoute(builder: (ctx) => CategoriesScreen()),
    );
  }
}
