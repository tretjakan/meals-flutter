import 'package:flutter/material.dart';
import '../favorites/favorites.screen.dart';
import '../categories/categories.screen.dart';

class TabScreen extends StatefulWidget {
  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  final List<Map<String, dynamic>> _pages = [
    { 'page': CategoriesScreen(), 'title': 'Categories' },
    { 'page': FavoritesScreen(), 'title': 'Your Favorites Meals' },
  ];
  int _selected = 0;
  void _onChange(int index) {
    setState(() {
      _selected = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _pages[_selected]['title'],
      ),
      body: _pages[_selected]['page'],
      bottomNavigationBar: BottomNavigationBar(
          onTap: _onChange,
          unselectedItemColor: Colors.white,
          selectedItemColor: Theme.of(context).accentColor,
          selectedFontSize: 18,
          unselectedFontSize: 12,
          type: BottomNavigationBarType.shifting,
          currentIndex: _selected,
          items: [
            BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.category),
              title: Text('Categories'),
            ),
            BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.star),
              title: Text('Favorites'),
            ),
          ]),
    );
  }
}
